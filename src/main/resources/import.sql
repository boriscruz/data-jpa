INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (1, 'Rocio', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (2, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (3, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (4, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (5, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (6, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (7, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (8, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (9, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (10, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (11, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (12, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (13, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (14, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (15, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (16, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (17, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (18, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (19, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (20, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (21, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (22, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (23, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (24, 'Roo', 'Rodriguez', 'roo32@gmail.com','2020-01-27','');
INSERT INTO customers (id, name, lastname, email, create_at, photo) VALUES (25, 'Bel', 'Soto', 'bel232@gmail.com','2020-01-27','');

/* Populate goods table */
INSERT INTO goods (name, price, create_at) VALUES('panasonic LCD TV', 2299990, NOW());
INSERT INTO goods (name, price, create_at) VALUES('Xiaomi cellphone', 2299991, NOW());
INSERT INTO goods (name, price, create_at) VALUES('Microwave Samsung', 2299992, NOW());

/* We create some invoices */
INSERT INTO invoices (description, observation, customer_id, create_at) VALUES ('Invoices equipment forniture', null, 1, NOW());
INSERT INTO items_invoices (quantity, invoice_id, goods_id) VALUES (1,1,1);
INSERT INTO items_invoices (quantity, invoice_id, goods_id) VALUES (4,1,2);
INSERT INTO items_invoices (quantity, invoice_id, goods_id) VALUES (5,1,3);

INSERT INTO users (username, password, enabled) VALUES ('admin', '$2a$10$kGzOEGsniNz7e7ddGewr6OU1Df6XeSRkisda6ZXw0P8yHBYoHfM.u',1);
INSERT INTO users (username, password, enabled) VALUES ('roo', '$2a$10$wrll5d5a93BgyXXH4IXgceo.gddXbbVwCjDBDv/Sa3GaCeDtoO4MG',1);

INSERT INTO authorities (user_id, authority) VALUES (1,'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES (1,'ROLE_ADMIN');
INSERT INTO authorities (user_id, authority) VALUES (2,'ROLE_USER');
