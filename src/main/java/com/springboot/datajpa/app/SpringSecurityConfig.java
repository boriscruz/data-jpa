package com.springboot.datajpa.app;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.springboot.datajpa.app.auth.handler.LoginSuccessHandler;
import com.springboot.datajpa.app.models.services.JpaUserDetailsService;

@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JpaUserDetailsService userDetailsService;

	@Autowired
	private LoginSuccessHandler successHandler;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/", "/css/**", "/js/**", "/toList", "/api/customers/**").permitAll()
//			.antMatchers("/view/**").hasAnyRole("USER")
//			.antMatchers("/uploads/**").hasAnyRole("USER")
//			.antMatchers("/form/**").hasAnyRole("ADMIN")
//			.antMatchers("/delete/**").hasAnyRole("ADMIN")
//			.antMatchers("/invoice/**").hasAnyRole("ADMIN")
				.anyRequest().authenticated().and().formLogin().successHandler(successHandler).loginPage("/login")
				.permitAll().and().logout().permitAll().and().exceptionHandling().accessDeniedPage("/error_403");

	}

	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder builder) throws Exception {

		builder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);

//		builder.jdbcAuthentication()
//			.dataSource(dataSource)
//			.passwordEncoder(passwordEncoder)
//			.usersByUsernameQuery("SELECT username, password, enabled FROM users WHERE username=?")
//			.authoritiesByUsernameQuery("SELECT u.username, a.authority FROM authorities a INNER JOIN users u ON (a.user_id=u.id) WHERE u.username=?");

//		PasswordEncoder encoder = this.passwordEncoder;
//
//		UserBuilder users = User.builder().passwordEncoder(encoder::encode);
//
//		builder.inMemoryAuthentication().withUser(users.username("admin").password("1234").roles("ADMIN", "USER"))
//				.withUser(users.username("roo").password("1234").roles("USER"));

	}

}
