package com.springboot.datajpa.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.springboot.datajpa.app.models.entity.Customer;
import com.springboot.datajpa.app.models.entity.Goods;
import com.springboot.datajpa.app.models.entity.Invoice;
import com.springboot.datajpa.app.models.entity.ItemInvoice;
import com.springboot.datajpa.app.models.services.ICustomerService;

@Secured("ROLE_ADMIN")
@Controller
@RequestMapping("/invoice")
@SessionAttributes("invoice")
public class InvoiceController {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/view/{id}")
	public String view(@PathVariable(value="id") Long id, Model model, RedirectAttributes flash) {
		
		Invoice invoice = customerService.findInvoiceById(id);
		
		if (invoice == null) {
			flash.addAttribute("error", "This invoice doesn't exist in the data base");
			return "redirect:/toList";
		}
		
		model.addAttribute("title", "Invoice: ".concat(invoice.getDescription()));
		model.addAttribute("invoice", invoice);
		
		return "invoice/view";
	}

	@Autowired
	private ICustomerService customerService;

	@GetMapping(value = "/form/{idCustomer}")
	public String toCreate(@PathVariable(value = "idCustomer") Long idCustomer, Map<String, Object> model,
			RedirectAttributes flash) {

		Customer customer = customerService.findOne(idCustomer);

		if (customer == null) {
			flash.addFlashAttribute("error", "The customer doesn't exists in the data base");
			return "redirect:/toList";
		}

		Invoice invoice = new Invoice();

		invoice.setCustomer(customer);

		model.put("invoice", invoice);
		model.put("title", "Create invoice");

		return "invoice/form";
	}

	@GetMapping(value="/load-goods/{name}", produces = {"application/json"})
	public @ResponseBody List<Goods> loadGoods(@PathVariable String name) {
		return customerService.findByName(name);
	}
	
	@PostMapping("/form")
	public String save(@Valid Invoice invoice,
			BindingResult result,
			Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemId,
			@RequestParam(name = "quantity[]", required = false) Integer[] quantity,
			RedirectAttributes flash,
			SessionStatus status) {
		
		if (result.hasErrors()) {
			model.addAttribute("title", "Create invoice");
			return "invoice/form";
		}
		
		if (itemId == null || itemId.length == 0) {
			model.addAttribute("title", "Create invoice");
			model.addAttribute("error", "Error: Invoice can't have not lines");
			return "invoice/form";
		}
		
		for (int i = 0; i < itemId.length; i++) {
			Goods goods =  customerService.findGoodsById(itemId[i]);
			
			ItemInvoice line = new ItemInvoice();
			
			line.setQuantity(quantity[i]);
			line.setGoods(goods);
			invoice.addItemInvoice(line);
			
			log.info("Id: "+itemId[i].toString()+", quantity: "+quantity[i].toString());
			
		}
		
		customerService.saveInvoice(invoice);
		
		status.setComplete();
		
		flash.addFlashAttribute("success", "Invoice was created successfully");
		
		return "redirect:/view/"+invoice.getCustomer().getId();
	}
	
}
