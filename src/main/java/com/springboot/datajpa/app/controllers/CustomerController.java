package com.springboot.datajpa.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.springboot.datajpa.app.models.entity.Customer;
import com.springboot.datajpa.app.models.services.ICustomerService;
import com.springboot.datajpa.app.models.services.IUploadFileService;
import com.springboot.datajpa.app.util.paginator.PageRender;

@Controller
@SessionAttributes("customer")
public class CustomerController {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ICustomerService customerService;

	@Autowired
	private IUploadFileService uploadFileService;

	@Value("${uploads.paths}")
	private String imagePath;

	@Secured("ROLE_USER")
	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> viewPhoto(@PathVariable String filename) {

		Resource resource = null;

		try {
			resource = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource + "\"")
				.body(resource);
	}

	@Secured("ROLE_USER")
	@GetMapping(value = "/view/{id}")
	public String view(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {

		Customer customer = customerService.findOne(id);
		if (customer == null) {
			flash.addFlashAttribute("error", "The customer doesn't exist in the data base");
			return "redirect:/toList";
		}
		log.info(customer.toString());
		model.put("customer", customer);
		model.put("title", "Customer detail: " + customer.getName());

		return "view";
	}

	@GetMapping({ "/toList", "/" })
	public String toList(@RequestParam(name = "page", defaultValue = "0") int page, Model model, Authentication authentication, HttpServletRequest request) {
		Pageable pageRequest = PageRequest.of(page, 4);

		if (authentication != null) {
			log.info("Hello, the user ".concat(authentication.getName()));
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if (auth != null) {
			log.info("Utilizing SecurityContextHolder.getContext().getAuthentication(); Hello, the user ".concat(auth.getName()));
		}
		
		if (hasRole("ROLE_ADMIN")) {
			log.info("Hello. ".concat(auth.getName()).concat(" you have access to the system!"));
		} else {
			log.info("Hello. ".concat(auth.getName()).concat(" you don't have access to the system!"));
		}
		
		SecurityContextHolderAwareRequestWrapper securityContext = new SecurityContextHolderAwareRequestWrapper(request, "ROLE_");
		
		if (securityContext.isUserInRole("ADMIN")) {
			log.info("Utilizing SecurityContextHolderAwareRequestWrapper. Hello. ".concat(auth.getName()).concat(" you have access to the system!"));
		} else {
			log.info("Utilizing SecurityContextHolderAwareRequestWrapper. Hello. ".concat(auth.getName()).concat(" you don't have access to the system!"));
		}
		
		
		Page<Customer> customers = customerService.findAll(pageRequest);

		PageRender<Customer> pageRender = new PageRender<>("/toList", customers);

		model.addAttribute("title", "Customers List");
		model.addAttribute("customers", customers);
		model.addAttribute("page", pageRender);
		return "toList";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/form")
	public String create(Map<String, Object> model) {
		Customer customer = new Customer();
		model.put("customer", customer);
		model.put("title", "Customer form");
		return "form";
	}

	@Secured("ROLE_ADMIN")
	@PostMapping(value = "/form")
	public String save(@Valid Customer customer, BindingResult result, Model model,
			@RequestParam("file") MultipartFile photo, RedirectAttributes flash, SessionStatus status) {

		if (result.hasErrors()) {
			model.addAttribute("title", "Customer form");
			return "form";
		}

		if (!photo.isEmpty()) {
//			Path resourcesDirectory = Paths.get("src//main//resources//static/uploads");
//			String rootPath = imagePath;

			if (customer.getId() != null && customer.getPhoto() != null && customer.getPhoto().length() > 0) {
				log.info("Deleting photo's customer");
				uploadFileService.delete(customer.getPhoto());
			}

			String uniqueFileName = null;

			try {
				uniqueFileName = uploadFileService.copy(photo);
			} catch (IOException e) {
				e.printStackTrace();
			}

			flash.addFlashAttribute("info", "The image was uploaded correctly '" + uniqueFileName + "'");

			customer.setPhoto(uniqueFileName);

		}

		String flashMessage = (customer.getId() != null) ? "Customer was modified successfully!"
				: "Customer was created successfully!";
		customerService.save(customer);
		status.setComplete();
		flash.addFlashAttribute("success", flashMessage);
		return "redirect:toList";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/form/{id}")
	public String edit(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {

		Customer customer = null;

		if (id > 0) {
			customer = customerService.findOne(id);
			if (customer == null) {
				flash.addFlashAttribute("success", "The ID's customer doesn't exist in our database");
				return "redirect:/toList";
			}
		} else {
			flash.addFlashAttribute("error", "The ID's customer cannot be zero!");
			return "redirect:/toList";
		}
		model.put("customer", customer);
		model.put("title", "Edit customer");

		return "form";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
		if (id > 0) {
			Customer customer = customerService.findOne(id);
			customerService.delete(id);
			flash.addFlashAttribute("success", "Customer was deleted successfully!");

			if (uploadFileService.delete(customer.getPhoto()))
				flash.addAttribute("info", "Photo " + customer.getPhoto() + " has been deleted with success!");

		}
		return "redirect:/toList";
	}
	
	private boolean hasRole(String role) {
		
		SecurityContext context = SecurityContextHolder.getContext();
		
		if (context == null) {
			return false;
		}
		
		Authentication auth = context.getAuthentication();
		
		if (auth == null) {
			return false;
		}
		
		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
		
		if (authorities.stream().findAny().filter(x -> role.equals(x.getAuthority())).orElse(null) == null) {
			log.info("your role is ".concat(role));
			return false;
		}
		return true;
		
	}

}
