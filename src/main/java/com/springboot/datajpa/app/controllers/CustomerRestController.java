package com.springboot.datajpa.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.datajpa.app.models.entity.Customer;
import com.springboot.datajpa.app.models.services.ICustomerService;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

	@Autowired
	private ICustomerService customerService;
	
	@GetMapping(value = "/toList")
	public List<Customer> toList() {
		return customerService.findAll(); 
	}
	
}
