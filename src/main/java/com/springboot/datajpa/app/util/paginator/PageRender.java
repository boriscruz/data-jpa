package com.springboot.datajpa.app.util.paginator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

public class PageRender<T> {

	private String url;
	private Page<T> page;

	private int pagesTotal;

	private int numberElementsPerPage;

	private int currentPage;

	private List<PageItem> pages;

	public PageRender(String url, Page<T> page) {
		this.url = url;
		this.page = page;
		this.pages = new ArrayList<PageItem>();
		numberElementsPerPage = page.getSize();
		pagesTotal = page.getTotalPages();
		currentPage = page.getNumber() + 1;

		int from, to;

		if (pagesTotal <= numberElementsPerPage) {
			from = 1;
			to = pagesTotal;
		} else {
			if (currentPage <= numberElementsPerPage / 2) {
				from = 1;
				to = numberElementsPerPage;
			} else if (currentPage >= pagesTotal - numberElementsPerPage / 2) {
				from = pagesTotal - numberElementsPerPage + 1;
				to = numberElementsPerPage;
			} else {
				from = currentPage - numberElementsPerPage / 2;
				to = numberElementsPerPage;
			}
		}

		for (int i = 0; i < to; i++) {
			pages.add(new PageItem(from + i, currentPage == from + i));
		}

	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Page<T> getPage() {
		return page;
	}

	public void setPage(Page<T> page) {
		this.page = page;
	}

	public int getPagesTotal() {
		return pagesTotal;
	}

	public void setPagesTotal(int pagesTotal) {
		this.pagesTotal = pagesTotal;
	}

	public int getNumberElementsPerPage() {
		return numberElementsPerPage;
	}

	public void setNumberElementsPerPage(int numberElementsPerPage) {
		this.numberElementsPerPage = numberElementsPerPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public List<PageItem> getPages() {
		return pages;
	}

	public void setPages(List<PageItem> pages) {
		this.pages = pages;
	}

	public boolean isFirst() {
		return page.isFirst();
	}

	public boolean isLast() {
		return page.isLast();
	}

	public boolean isHasNext() {
		return page.hasNext();
	}
	
	public boolean isHasPrevious() {
		return page.hasPrevious();
	}

}
