package com.springboot.datajpa.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.datajpa.app.models.entity.User;

public interface IUserDAO extends CrudRepository<User, Long> {

	public User findByUsername(String username);

}
