package com.springboot.datajpa.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.datajpa.app.models.entity.Invoice;

public interface IInvoiceDAO extends CrudRepository<Invoice, Long> {

	
	
}
