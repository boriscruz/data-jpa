package com.springboot.datajpa.app.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.springboot.datajpa.app.models.entity.Customer;
import com.springboot.datajpa.app.models.entity.Goods;
import com.springboot.datajpa.app.models.entity.Invoice;

public interface ICustomerService {

	public List<Customer> findAll();

	public Page<Customer> findAll(Pageable pageable);

	public void save(Customer customer);

	public Customer findOne(Long id);

	public void delete(Long id);
	
	public List<Goods>  findByName(String name);
	
	public void saveInvoice(Invoice invoice);
	
	public Goods findGoodsById(Long id);
	
	public Invoice findInvoiceById(Long id);
}
