package com.springboot.datajpa.app.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.datajpa.app.models.dao.ICustomerDAO;
import com.springboot.datajpa.app.models.dao.IGoodsDAO;
import com.springboot.datajpa.app.models.dao.IInvoiceDAO;
import com.springboot.datajpa.app.models.entity.Customer;
import com.springboot.datajpa.app.models.entity.Goods;
import com.springboot.datajpa.app.models.entity.Invoice;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	ICustomerDAO customerDAO;
	
	@Autowired
	private IGoodsDAO goodsDAO;
	
	@Autowired
	private IInvoiceDAO invoiceDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Customer> findAll() {
		return (List<Customer>) customerDAO.findAll();
	}

	@Override
	@Transactional
	public void save(Customer customer) {
		customerDAO.save(customer);
	}

	@Override
	@Transactional(readOnly = true)
	public Customer findOne(Long id) {
		return customerDAO.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		customerDAO.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Customer> findAll(Pageable pageable) {
		return customerDAO.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Goods> findByName(String name) {
		return goodsDAO.findByNameLikeIgnoreCase("%"+name+"%");
	}

	@Override
	@Transactional
	public void saveInvoice(Invoice invoice) {
		invoiceDAO.save(invoice);
	}

	@Override
	@Transactional(readOnly = true)
	public Goods findGoodsById(Long id) {
		return goodsDAO.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Invoice findInvoiceById(Long id) {
		return invoiceDAO.findById(id).orElse(null);
	}

}
