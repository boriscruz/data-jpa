package com.springboot.datajpa.app.models.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.datajpa.app.models.dao.IUserDAO;
import com.springboot.datajpa.app.models.entity.User;

@Service("jpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(JpaUserDetailsService.class);

	@Autowired
	private IUserDAO userDAO;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userDAO.findByUsername(username);

		if (user == null) {
			logger.error("Login error: this username " + user.getUsername() + " don't exists in the system");
			throw new UsernameNotFoundException("Username: " + user.getUsername() + " don't exists in the system");
		}

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		user.getRoles().forEach(role -> {
			authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
		});

		if (authorities.isEmpty()) {
			logger.error("This username " + user.getUsername() + " don't have any role");
			throw new UsernameNotFoundException("Username: " + user.getUsername() + " don't have any role");
		}

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				user.getEnabled(), true, true, true, authorities);
	}

}
