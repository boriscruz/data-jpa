package com.springboot.datajpa.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.springboot.datajpa.app.models.entity.Goods;

public interface IGoodsDAO extends CrudRepository<Goods, Long> {

	@Query("SELECT g FROM Goods g WHERE g.name LIKE %?1%")
	public List<Goods> findByName(String name);
	
	public List<Goods> findByNameLikeIgnoreCase(String name);
	
}
