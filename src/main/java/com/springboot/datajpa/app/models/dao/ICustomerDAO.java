package com.springboot.datajpa.app.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.springboot.datajpa.app.models.entity.Customer;

public interface ICustomerDAO extends PagingAndSortingRepository<Customer, Long> {

}
